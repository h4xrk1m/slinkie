"""
The expected result of running this script is an endless stream of

I'm not your buddy, friend!
He's not your friend, guy!
I'm not your guy, buddy!
He's not your buddy, friend!
I'm not your friend, guy!
He's not your guy, buddy!
I'm not your buddy, friend!
He's not your friend, guy!

Source:
    South Park episode "Canada on Strike"
    Season 12, Episode 4

Clip:
    https://www.youtube.com/watch?v=iH3K2rkkU7g
"""

from time import sleep
from itertools import cycle                 
from slinkie import Slinkie                 

template = "{who} not your {a}, {b}!"
who = cycle(("I'm", "He's"))
canadian = cycle(('buddy', 'friend', 'guy'))  # We'll sweep(2) this.

conversation = (
    Slinkie(canadian)
        .sweep(2)
        .smap(lambda a, b: template.format(who=next(who), a=a, b=b))
)

while True:
    print(next(conversation))
    sleep(1)
